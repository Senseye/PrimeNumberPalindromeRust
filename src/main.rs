use std::usize;
use std::time::SystemTime;
use std::vec::Vec;

const MAX: usize = 999999999;

struct Result {
    palindrome: usize,
    left: usize,
    rigth: usize,
}

fn main() {
    let start_time = SystemTime::now();
    let result = find_result();
    let difference = SystemTime::now().duration_since(start_time);

    println!("Palindrome: {:?}", result.palindrome);
    println!("Prime numbers: {:?} * {:?}", result.left, result.rigth);
    println!("Duration: {:?}", difference);
}

fn find_result() -> Result {
    let primes = get_primes(10000, 99999);

    let length = primes.len();

    let mut result = Result {
        palindrome: 0,
        left: 0,
        rigth: 0,
    };

    for i in 0..length {
        let from = i + 1;

        for j in from..length {
            let left = primes[i];
            let right = primes[j];
            let possible = left * right;

            if possible > MAX {
                continue
            }

            if possible > result.palindrome && is_palindrome(possible) {
                result.palindrome = possible;
                result.left = left;
                result.rigth = right;
            }
        }
    }

    return result;
}

fn get_primes(from: usize, to: usize) -> Vec<usize> {
    let size: usize = to + 1;

    let mut variants: Vec<bool> = Vec::with_capacity(size);

    for _ in 0..size {
        variants.push(true)
    }

    let limit = (to as f64).sqrt() as usize;

    for i in 2..limit {
        if variants[i] {
            let mut j = i * i;
            while j < size {
                variants[j] = false;

                j += i;
            }
        }
    }

    let mut result: Vec<usize> = Vec::with_capacity(size);

    for i in from..size {
        if variants[i] {
            result.push(i);
        }
    }

    return result;
}

fn is_palindrome(mut value: usize) -> bool {
    let mut digits = [0; 9];

    let mut i: usize = 0;
    while value > 0 {
        digits[i] = value % 10;
        i += 1;

        value /= 10;
    }

    let limit: usize = i >> 1;
    let right: usize = i - 1;

    for j in 0..limit {
        if digits[j] != digits[right - j] {
            return false;
        }
    }

    return true;
}
